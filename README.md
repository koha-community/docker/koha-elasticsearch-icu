# koha-elasticsearch-icu

This repository handles Koha's custom Elasticsearch and OpenSearch container
images build.

Koha requires the `ICU analysis` plugin installed. Installing the plugin requires
network access and, as such, it is subject to temporal failures. In order to minimize
such issues for our test infrastructure, we bundle the required extras on top
of the official images.

The main repository is empty, and each ES/OS version is handled on its own branch:

* [5.x](https://gitlab.com/koha-community/docker/koha-elasticsearch-icu/-/tree/5.x)
* [6.x](https://gitlab.com/koha-community/docker/koha-elasticsearch-icu/-/tree/6.x)
* [7.x](https://gitlab.com/koha-community/docker/koha-elasticsearch-icu/-/tree/7.x)
* [7.x-arm64](https://gitlab.com/koha-community/docker/koha-elasticsearch-icu/-/tree/7.x-arm64)
* [7.x-os](https://gitlab.com/koha-community/docker/koha-elasticsearch-icu/-/tree/7.x-os)
* [8.x](https://gitlab.com/koha-community/docker/koha-elasticsearch-icu/-/tree/8.x)
* [8.x-arm64](https://gitlab.com/koha-community/docker/koha-elasticsearch-icu/-/tree/8.x-arm64)
* [opensearch-1.x](https://gitlab.com/koha-community/docker/koha-elasticsearch-icu/-/tree/opensearch-1.x)
* [opensearch-1.x-arm64](https://gitlab.com/koha-community/docker/koha-elasticsearch-icu/-/tree/opensearch-1.x-arm64)
* [opensearch-2.x](https://gitlab.com/koha-community/docker/koha-elasticsearch-icu/-/tree/opensearch-2.x)
* [opensearch-2.x-arm64](https://gitlab.com/koha-community/docker/koha-elasticsearch-icu/-/tree/opensearch-2.x-arm64)
